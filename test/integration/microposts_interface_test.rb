# frozen_string_literal: true

require 'test_helper'

class MicropostsInterfaceTest < ActionDispatch::IntegrationTest

  CONTENT = 'This micropost really ties the room together'

  def setup
    @user = users(:michael)
    @other_user = users(:archer)
    @another_user = users(:malory)
  end

  test 'micropost interface' do
    log_in_as @user

    get root_path
    assert_select 'div.pagination', 1
    assert_select 'input[type=file]'

    assert_no_difference 'Micropost.count' do
      post microposts_path, params: {
        micropost: {
          content: ''
        }
      }
    end

    assert_select 'div#error_explanation'

    assert_difference 'Micropost.count', 1 do
      post microposts_path, params: {
        micropost: {
          content: CONTENT,
          picture: fixture_file_upload(
            'test/fixtures/files/rails.png',
            'image/png'
          )
        }
      }
    end

    assert assigns(:micropost).picture?
    assert_redirected_to root_path

    follow_redirect!
    assert_match CONTENT, response.body
    assert_select 'a', text: 'delete'

    first_micropost = @user.microposts.paginate(page: 1).first
    assert_difference 'Micropost.count', -1 do
      delete micropost_path(first_micropost)
    end

    get user_path(@other_user)
    assert_select 'a', text: 'delete', count: 0
  end

  test 'micropost sidebar count' do
    log_in_as @user

    get root_path
    assert_match "#{@user.microposts.count} microposts", response.body

    log_in_as @another_user

    get root_path
    assert_match '0 microposts', response.body
    @another_user.microposts.create!(content: 'A micropost')

    get root_path
    assert_match '1 micropost', response.body
  end

end
