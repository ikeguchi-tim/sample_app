require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  NAME = 'Foo Bar'.freeze
  EMAIL = 'foo@bar.com'.freeze

  def setup
    @user = users(:michael)
  end

  test 'unsuccessful edit' do
    log_in_as @user

    get edit_user_path(@user)
    assert_template 'users/edit'

    patch user_path(@user), params: {
      user: {
        name: '',
        email: 'foo@invalid',
        password: 'foo',
        password_confirmaion: 'bar'
      }
    }

    assert_template 'users/edit'
  end

  test 'successful edit with friendly forwarding' do
    get edit_user_path(@user)

    log_in_as @user
    assert_redirected_to edit_user_path(@user)

    follow_redirect!
    assert_template 'users/edit'

    patch user_path(@user), params: {
      user: {
        name: NAME,
        email: EMAIL,
        password: '',
        password_confirmation: ''
      }
    }

    assert_not flash.empty?
    assert_redirected_to @user

    @user.reload
    assert_equal NAME, @user.name
    assert_equal EMAIL, @user.email
  end

end
